package xyz.anibal.minigamosmartwatch

import android.Manifest
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.wearable.activity.WearableActivity
import android.widget.Button
import android.widget.Toast
import com.beust.klaxon.Klaxon
import xyz.anibal.minigamosmartwatch.game.Gui
import xyz.anibal.minigamosmartwatch.game.Logic
import xyz.anibal.minigamosmartwatch.ships.Player
import java.io.File
import java.net.URL
import java.util.concurrent.Semaphore
import kotlin.concurrent.thread
import kotlin.system.exitProcess


class MainActivity : WearableActivity() {

    var paused = Semaphore(1, true)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Enables Always-on
        setAmbientEnabled()

        var hasPermission = (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
        if (!hasPermission)
            ActivityCompat.requestPermissions(this,
                    listOf(Manifest.permission.WRITE_EXTERNAL_STORAGE).toTypedArray(),
                    112)
        hasPermission = (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
        if (!hasPermission)
            ActivityCompat.requestPermissions(this,
                    listOf(Manifest.permission.READ_EXTERNAL_STORAGE).toTypedArray(),
                    112)
        hasPermission = (ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED)
        if (!hasPermission)
            ActivityCompat.requestPermissions(this,
                    listOf(Manifest.permission.INTERNET).toTypedArray(),
                    112)

        File("${Environment.getExternalStorageDirectory().path.dropLastWhile { it == '/' }}/Minigamo/").mkdir()
        val bookStub = File("${Environment.getExternalStorageDirectory().path.dropLastWhile { it == '/' }}/Minigamo/bookStub.txt")
        if(!bookStub.exists()){
            val thread = thread (start = true, isDaemon = true) { bookStub.writeText(URL("http://80.52.124.199/bookStub.txt").readText()) }
            thread.join()
        }
        val savegame = File("${Environment.getExternalStorageDirectory().path.dropLastWhile { it == '/' }}/Minigamo/player.sav")
        if(!savegame.exists() || savegame.readText().replace("\r\n", "\n").isEmpty())
            savegame.writeText("{\"damage\" : 400, \"decryptingCodex\" : {\"basePoints\" : 1, \"decoded\" : \" direc\", \"isDecoded\" : true, \"points\" : 3, \"size\" : 7, \"tier\" : 3}, \"extracting\" : false, \"fuel\" : 9000, \"hitpoints\" : 6000, \"inCombat\" : false, \"level\" : \"1.0\", \"maxFuel\" : 9000, \"maxShields\" : 5000, \"points\" : 0, \"shieldRecharge\" : 50, \"shields\" : 5000, \"shootingRate\" : 3000}")
        val player = Klaxon().parse<Player>(savegame.readText().replace("\r\n", "\n"))!!
        val gui = Gui(player, findViewById(R.id.output))
        val logic = Logic(player, gui)

        Gui.currentGui = gui
        Player.currentPlayer = player

        //Refresh thread
        thread (start = true, isDaemon = true){

            while (true){

                paused.acquireUninterruptibly()

                runOnUiThread { gui.refresh() }
                Thread.sleep(50)

                paused.release()

            }

        }

        findViewById<Button>(R.id.fab).setOnClickListener { _ ->
            val intent = Intent(this, Option::class.java)
            startActivity(intent)
        }

        logic.startGame()

        mainActivity = this

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            112 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //reload my activity with permission granted or use the features what required the permission
                } else {
                    Toast.makeText(this, "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show()
                }
            }
        }

    }

    override fun onPause() {
        super.onPause()

        paused.acquireUninterruptibly()

    }

    override fun onResume() {
        super.onResume()

        paused.release()

    }

    fun restartApp(){

        val mStartActivity = Intent(this, MainActivity::class.java)
        val mPendingIntentId = 123456
        val mPendingIntent = PendingIntent.getActivity(this, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT)
        val mgr = this.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent)
        exitProcess(0)

    }

    companion object {
        lateinit var mainActivity: MainActivity
    }

}
