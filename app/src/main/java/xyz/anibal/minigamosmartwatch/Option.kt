package xyz.anibal.minigamosmartwatch

import android.os.Bundle
import android.os.Environment
import android.support.wearable.activity.WearableActivity
import android.widget.Button
import com.beust.klaxon.Klaxon
import xyz.anibal.minigamosmartwatch.game.Gui
import xyz.anibal.minigamosmartwatch.game.Logic
import xyz.anibal.minigamosmartwatch.ships.Player
import java.io.File
import java.lang.Thread.sleep
import kotlin.concurrent.thread


class Option : WearableActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_option)

        val button1 = findViewById<Button>(R.id.button1)
        val button2 = findViewById<Button>(R.id.button2)
        val button3 = findViewById<Button>(R.id.button3)
        val button4 = findViewById<Button>(R.id.button4)
        val button5 = findViewById<Button>(R.id.button5)
        val button6 = findViewById<Button>(R.id.button6)
        val button7 = findViewById<Button>(R.id.button7)
        val button8 = findViewById<Button>(R.id.button8)
        val button9 = findViewById<Button>(R.id.button9)
        val button10 = findViewById<Button>(R.id.button10)
        val buttonJump = findViewById<Button>(R.id.buttonJump)
        val buttonSave = findViewById<Button>(R.id.buttonSave)
        val buttonBot = findViewById<Button>(R.id.buttonBot)
        val buttonExit = findViewById<Button>(R.id.exit)

        // Set button actions
        button1.setOnClickListener { Logic.selectedOption = 1; sleep(10); onBackPressed()}
        button2.setOnClickListener { Logic.selectedOption = 2; sleep(10); onBackPressed()}
        button3.setOnClickListener { Logic.selectedOption = 3; sleep(10); onBackPressed()}
        button4.setOnClickListener { Logic.selectedOption = 4; sleep(10); onBackPressed()}
        button5.setOnClickListener { Logic.selectedOption = 5; sleep(10); onBackPressed()}
        button6.setOnClickListener { Logic.selectedOption = 6; sleep(10); onBackPressed()}
        button7.setOnClickListener { Logic.selectedOption = 7; sleep(10); onBackPressed()}
        button8.setOnClickListener { Logic.selectedOption = 8; sleep(10); onBackPressed()}
        button9.setOnClickListener { Logic.selectedOption = 9; sleep(10); onBackPressed()}
        button10.setOnClickListener { Logic.selectedOption = 10; sleep(10); onBackPressed()}
        buttonJump.setOnClickListener {

            thread (start = true, isDaemon = true){
                if(Player.currentPlayer.inCombat){

                    Player.currentPlayer.decryptingCodex = null

                    Gui.currentGui.event = "Emergency Extraction!  Unlinking the Integrated Decryptor"
                    Thread.sleep(1000)
                    Gui.currentGui.event += "."
                    Thread.sleep(1000)
                    Gui.currentGui.event += "."
                    Thread.sleep(1000)
                    Gui.currentGui.event += "."
                    Thread.sleep(1000)

                    Gui.currentGui.event = "Charging Jump Drive Coils"
                    Thread.sleep(1000)
                    Gui.currentGui.event += "."
                    Thread.sleep(1000)
                    Gui.currentGui.event += "."
                    Thread.sleep(1000)
                    Gui.currentGui.event += "."
                    Thread.sleep(1000)
                    Gui.currentGui.event += "."
                    Thread.sleep(1000)
                    Gui.currentGui.event += "."
                    Thread.sleep(500)


                    Gui.currentGui.event = "Jumping"

                    Thread.sleep(1000)

                    Player.currentPlayer.extracting = true

                }else{

                    val current = Gui.currentGui.event
                    Gui.currentGui.event = "Not in an emergency situation"
                    Thread.sleep(2000)
                    Gui.currentGui.event = current

                }
            }

            onBackPressed()

        }
        buttonSave.setOnClickListener {

            if(!Player.currentPlayer.inCombat)
                File("${Environment.getExternalStorageDirectory().path.dropLastWhile { it == '/' }}/Minigamo/player.sav").writeText(Klaxon().toJsonString(Player.currentPlayer))

            onBackPressed()

        }
        buttonBot.setOnClickListener {

            thread(start = true, isDaemon = true){
                Logic.botMode = !Logic.botMode
                val current = Gui.currentGui.event
                Gui.currentGui.event = "Bot mode is now ${if (Logic.botMode) "ON" else "OFF"}"
                Thread.sleep(2000)
                Gui.currentGui.event = current
            }

            onBackPressed()

        }
        buttonExit.setOnClickListener {

            finishAndRemoveTask()
            System.exit(0)

        }

    }

}
