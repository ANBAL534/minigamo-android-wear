package xyz.anibal.minigamosmartwatch.game

import xyz.anibal.minigamosmartwatch.ships.Player
import xyz.anibal.minigamosmartwatch.station.Station
import xyz.anibal.minigamosmartwatch.text.Book
import xyz.anibal.minigamosmartwatch.text.Codex
import java.util.*

data class Planet (val name: String, val player: Player, val gui: Gui){

    val codex: Codex? = if(Random().nextInt(10) > 5) Book.getABook().getRandomCodex() else null
    val fuel = if(codex == null) Random().nextInt(500)+1 else (codex.basePoints.toFloat()*(Random().nextFloat()+0.5)).toInt()
    val station = if(codex == null) if(Random().nextInt(3) == 2) Station(player, gui) else null else null

}